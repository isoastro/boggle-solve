# boggle-solve

1. Generates valid Boggle games
2. Generates, stores, and supports querying of tries that contain the most common English words (that are still valid Boggle words)
3. Rapidly solves Boggle games by recursively querying the board and a parallel trie of words
